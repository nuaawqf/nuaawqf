version=5.8.3openmpi
language=Fortran 90
compiler_env=VT_FC
compiler_flags_env=VT_FCFLAGS
compiler=
compiler_flags=
linker_flags=
libs= -lotf  -lz      -ldl  
includedir=${includedir}
libdir=${libdir}
vtlib=-lvt
vtmpilib=-lmpi_f77 -lvt-mpi  
vtmtlib=-lvt-mt 
vthyblib=-lmpi_f77 -lvt-hyb   
vtpomplib=-lvt-pomp
vtdynattlib=
opari_bin=/home/darrin/OpenFOAM/ThirdParty-2.1.1/platforms/linux64Gcc/openmpi-1.5.3/bin/opari
opari_tab_compiler=gcc
opari_tab_compiler_flags=-m64 -fPIC
inst_compiler_flags=-g -finstrument-functions
inst_avail=manual compinst
inst_default=compinst
